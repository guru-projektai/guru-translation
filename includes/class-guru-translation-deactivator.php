<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://gggroup.media
 * @since      1.0.0
 *
 * @package    Guru_Translation
 * @subpackage Guru_Translation/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Guru_Translation
 * @subpackage Guru_Translation/includes
 * @author     GG Group Media <info@gggroup.media>
 */
class Guru_Translation_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
