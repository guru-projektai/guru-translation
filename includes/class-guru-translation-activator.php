<?php

/**
 * Fired during plugin activation
 *
 * @link       https://gggroup.media
 * @since      1.0.0
 *
 * @package    Guru_Translation
 * @subpackage Guru_Translation/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Guru_Translation
 * @subpackage Guru_Translation/includes
 * @author     GG Group Media <info@gggroup.media>
 */
class Guru_Translation_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
